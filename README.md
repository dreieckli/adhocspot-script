# adhocspot-script

Bash script to easily configure your interface to share your internet connection and configure a DHCP and DNS and TFTP boot server to listen on it. IP, DHCP, DNS can be configured, and for WiFi interfaces also wireless mode and encryption.


## Dependencies

On Arch Linux, this script depends on the following packages (in brackets the bare commands it uses):

* bash: For running this script. (`bash`)
* dnsmasq: For DHCP and DNS. (`dnsmasq`)
* iptables: For configuring NAT/ masquerading. (`iptables`)
* net-tools: For configuring IP information of the network interface. (`ifconfig`)
* wireless_tools: For configuration of wireless network interface. (`iwconfig`)
* wpa_supplicant: For WPA-encryption. (`wpa_supplicant`, `wpa_passphrase`)
* &hellip; and standard tools like `grep`, `sed`, `awk` &hellip;


## Usage

Output from `./adhocspot.sh -h`, version *20190514.6* (which is the up to date version as of 2022-02-07, 21:12:16 UTC):

```
Usage:
  ./adhocspot.sh action [arguments ...]

Actions (exactly one required):
  up   | start                      Start the thing.
  down | stop                       Stop the thing.
  stat | status | state | show      Show the status of the thing.
  -h   | --help | help              Print this message and exit.
  -V   | --version                  Print version number and exit. (Version is: '20190514.6'.)

Arguments (all optional):
  -h   | --help | help              Print this message and exit.
  -V   | --version                  Print version number and exit. (Version is: '20190514.6'.)
  -v   | --verbose                  Print information as we go on/ start daemons verbosely.
  -d   | --debug                    Print debug output/ start daemons with debug output.
                                    Implies verbose.
  -i   | --iface <iface>            Interface on which the connection should be made available
                                    (default: wlan0).
                                    Only characters out of the set [a-zA-Z0-9\._\-] are allowed.
  -ip  | --ip <ip>                  IPv4-address to configure this interface to (default: 192.168.101.3).
  -nm  | --netmask <netmask>        Netmask to use on this interface (default: 255.255.255.0).
  -m   | --mac <MAC-address>        Set the MAC-address of this interface (defaults to the
                                    interface's native MAC-address).
  -ni  | --no-ipconfig              If specified, do not configure IP information for this
                                    interface. Useful e.g. if already configured.
                                    Specifying this option, the following won't be configured:
                                    IP-address, netmask, MAC-address.
  -o   | --out-iface <iface>        If specified, configure masquerading ('NAT') only for
                                    packages leaving on this interface (usually this is your
                                    interface which connects to the internet, not the one
                                    specified by the option '-i' / '--iface').
                                    Only characters out of the set [a-zA-Z0-9\._\-] are allowed.
                                    If not specified, NAT will be configured on all available non-
                                    local interfaces (determined by name starting with 'lo').
  -nn  | --no-nat                   If specified, do not configure and deconfigure network address
                                    translation, forwarding and masquerading.
  -dl  | --dhcp-lower <dhcp-ip>     Lower end of the range of IP-addresses to assign to
                                    clients. The default is the first three numbers of our
                                    IP-address, and then 121, e.g. 192.168.101.121
  -du  | --dhcp-upper <dhcp-ip>     Upper end of the range of IP-addresses to assign to
                                    clients. The default is the first three numbers of our
                                    IP-address, and then 199, e.g. 192.168.101.199
  -wm  | --wifi-mode <mode>         The WiFi-mode to set the interface to. Allowed modes:
                                    'ad-hoc', 'master', 'managed'. (Default: ad-hoc.)
  -e   | --essid <ESSID>            Set the WiFi ESSID to use (default: adhoc_network).
  -c   | --channel <wifi-channel>   Set the WiFi channel to use (default: 8).
  -enc | --enc <encryption-type>    Set the type of WiFi encryption to use. Possible values:
                                    'off', 'wep', 'wpa'. (Default: off).
  -key | --key <enc-password>       Set the password to use for the WiFi encryption key
                                    (default: please_specify_a_non_default_key).
                                    NOTE: For WEP, the password has to be 5 or 13 characters long.
                                          For WPA, it has to be between 8 ans 63 characters long.
  -nw  | --no-wifi                  If specified, do not wifi-configure the interface. Useful
                                    e.g. if already configured or it's not a WiFi interface.
                                    Specifying this option, the following won't be configured:
                                    WiFi-mode, ESSID, channel, encryption type, encryption key.
  -tf  | --tftp-root <dir>          Directory where to serve files for TFTP network boot from
                                    (default: /tftpboot)
  -nt  | --no-tftp                  If specified, do not provide a TFTP server.
  -r   | --rundir <directory>       Where to store and look for runtime information
                                    (default: /var/run/adhocspot). It get's created if nonextisting.

(Info: We were started at 2022-02-07_21-12-16.)
```


## License

This software is licensed under the GNU General Public License (GPL), version 3.0. See the file [`COPYING.GPL3.txt`](./COPYING.GPL3.txt) within the repository for the license text.
